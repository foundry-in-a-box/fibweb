install:
	virtualenv .env --distribute --prompt=\(fibweb\)
	. `pwd`/.env/bin/activate; pip install -r requirements.txt
	bower install
	mkdir uploads
	mkdir output
	mkdir fontpackages

serve:
	. `pwd`/.env/bin/activate; python fibweb.py

