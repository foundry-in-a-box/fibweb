#!fibui/bin/python
from flask import Flask, Response, Markup, render_template, request, send_from_directory, jsonify
import os
import markdown
from werkzeug import secure_filename
from makefontpkg import process_repo

UPLOAD_FOLDER = 'uploads'
OUTPUT_FOLDER = 'output'
FONTPKG_FOLDER = 'fontpackages'
ALLOWED_EXTENSIONS = set(['ttf', 'otf', 'woff', 'eot', 'sfd', 'svg'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER


# UTIL FUNCTIONS ##########################################

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def get_filename_base():
    import time
    timestr = time.strftime("%Y%m%d-%H%M%S")
    import random
    randstr = "%016x" % random.getrandbits(64)
    return timestr + '-' + randstr


# ROUTES ##################################################

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/transpacing', methods=['GET', 'POST'])
def transpacing():
    if request.method == 'POST':
        if request.files:
            glyph_font = request.files['glyphfont[]']
            spacing_font = request.files['spacingfont[]']
            if glyph_font and allowed_file(glyph_font.filename):
                g_filename = secure_filename(glyph_font.filename)
                glyph_font.save(os.path.join(app.config['UPLOAD_FOLDER'], g_filename))
            if spacing_font and allowed_file(spacing_font.filename):
                s_filename = secure_filename(spacing_font.filename)
                spacing_font.save(os.path.join(app.config['UPLOAD_FOLDER'], s_filename))
            g_path = os.path.join(app.config['UPLOAD_FOLDER'], g_filename)
            s_path = os.path.join(app.config['UPLOAD_FOLDER'], s_filename)
            out_filename = os.path.join(app.config['OUTPUT_FOLDER'], get_filename_base() + '.ttf')
            cmd = "/usr/bin/python tools/transpacing %s %s %s" % (s_path, g_path, out_filename)
            import subprocess
            subprocess.call(cmd, shell=True)
            out_filename_img = out_filename + ".png"
            cmd = "fontimage %s -o %s" % (out_filename, out_filename_img)
            subprocess.call(cmd, shell=True)
            return jsonify(
                {"result":
                    {"file_url": out_filename,
                     "img_url": '/' + out_filename_img}
                 }
            )
        else:
            return "No files input! Please try again"
    helptext = Markup(markdown.markdown(open('content/help-transpacing.md', 'r').read()))
    return render_template('transpacing.html', **locals())


@app.route('/outline', methods=['GET', 'POST'])
def outline():
    if request.method == 'POST':
        if request.files:
            font = request.files['font[]']
            if font and allowed_file(font.filename):
                filename = secure_filename(font.filename)
                font.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            font_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            out_filename = os.path.join(app.config['OUTPUT_FOLDER'], get_filename_base() + '.ttf')
            cmd = "fontforge -script tools/outline.pe %s %s %s" % (font_path, out_filename, request.form['width'])
            import subprocess
            subprocess.call(cmd, shell=True)
            out_filename_img = out_filename + ".png"
            cmd = "fontimage %s -o %s" % (out_filename, out_filename_img)
            subprocess.call(cmd, shell=True)
            return jsonify(
                {"result":
                    {"file_url": out_filename,
                     "img_url": '/' + out_filename_img}
                 }
            )
        else:
            return "No files input! Please try again"
    helptext = Markup(markdown.markdown(open('content/help-outline.md', 'r').read()))
    return render_template('outline.html', **locals())


@app.route('/inline', methods=['GET', 'POST'])
def inline():
    if request.method == 'POST':
        if request.files:
            font = request.files['font[]']
            if font and allowed_file(font.filename):
                filename = secure_filename(font.filename)
                font.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            font_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            out_filename = os.path.join(app.config['OUTPUT_FOLDER'], get_filename_base() + '.ttf')
            cmd = "fontforge -script tools/inline.pe %s %s %s %s" % (font_path, out_filename, request.form['width'], request.form['gap'])
            import subprocess
            subprocess.call(cmd, shell=True)
            out_filename_img = out_filename + ".png"
            cmd = "fontimage %s -o %s" % (out_filename, out_filename_img)
            subprocess.call(cmd, shell=True)
            return jsonify(
                {"result":
                    {"file_url": out_filename,
                     "img_url": '/' + out_filename_img}
                 }
            )
        else:
            return "No files input! Please try again"
    helptext = Markup(markdown.markdown(open('content/help-inline.md', 'r').read()))
    return render_template('inline.html', **locals())


@app.route('/shadow', methods=['GET', 'POST'])
def shadow():
    if request.method == 'POST':
        if request.files:
            font = request.files['font[]']
            if font and allowed_file(font.filename):
                filename = secure_filename(font.filename)
                font.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            font_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            out_filename = os.path.join(app.config['OUTPUT_FOLDER'], get_filename_base() + '.ttf')
            cmd = "fontforge -script tools/shadow.pe %s %s %s %s %s" % (font_path, out_filename, request.form['angle'], request.form['width'], request.form['shadowwidth'])
            import subprocess
            subprocess.call(cmd, shell=True)
            out_filename_img = out_filename + ".png"
            cmd = "fontimage %s -o %s" % (out_filename, out_filename_img)
            subprocess.call(cmd, shell=True)
            return jsonify(
                {"result":
                    {"file_url": out_filename,
                     "img_url": '/' + out_filename_img}
                 }
            )
        else:
            return "No files input! Please try again"
    helptext = Markup(markdown.markdown(open('content/help-shadow.md', 'r').read()))
    return render_template('shadow.html', **locals())


# Font packages ##########################################

@app.route('/fontpkg', methods=['GET'])
def fontpkg_index():
    packages = [fn.replace('.json', '') for fn in os.listdir(FONTPKG_FOLDER)]
    packages.sort()
    return render_template('fontpkg_index.html', packages=packages)


@app.route('/fontpkg/add', methods=['POST'])
def fontpkg_add():
    if request.form.get('repo'):
        pkg_name = process_repo(request.form['repo'])
        return jsonify(
            {"result":
                {'pkg_name': pkg_name,
                 'pkg_url': 'fontpkg/' + pkg_name}
             }
        )


@app.route('/fontpkg/<name>', methods=['GET'])
def fontpkg_view(name):
    filename = 'fontpackages/%s.json' % name
    if os.path.exists(filename):
        data = open(filename, 'r').read()
        # http://stackoverflow.com/a/26961568
        return Response(response=data, status=200, mimetype="application/json")


# Static paths ###########################################

@app.route('/bower_components/<path:filename>')
def custom_static(filename):
    return send_from_directory('bower_components', filename)


@app.route('/uploads/<path:filename>')
def custom_uploads(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


@app.route('/output/<path:filename>')
def custom_output(filename):
    return send_from_directory(app.config['OUTPUT_FOLDER'], filename)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, threaded=True)
