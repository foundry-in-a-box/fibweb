
Installation
------------

Install the necessary packages on your system; on Debian-based distros, this is:

    sudo aptitude install python-virtualenv make npm

Just run

    make install

You also need the `python-fontforge` package installed system-wide, since it can't be installed from pip.

Running
-------

Type

    make serve

And in your browser visit `http://localhost:5000`.
