import fontforge
import os
import fnmatch
import git
import json
import hashlib
from zenlog import log
from checksumdir import dirhash

repo_dir = 'repos/'
fontpkg_dir = 'fontpackages/'


def md5sum(filename, blocksize=65536):
    hash = hashlib.md5()
    if os.path.isdir(filename):
        return dirhash(filename)
    else:
        with open(filename, "rb") as f:
            for block in iter(lambda: f.read(blocksize), b""):
                hash.update(block)
        return hash.hexdigest()


def local_and_remote_are_at_same_commit(repo, remote):
    local_commit = repo.commit()
    remote_commit = remote.fetch()[0].commit
    return local_commit.hexsha == remote_commit.hexsha


def clone_or_update_repo(url):
    name = url.split('/')[-1].replace('.git', '')
    dir_name = os.path.join(repo_dir, name)

    # do we have a local copy?
    if os.path.isdir(dir_name):
        repo = git.Repo(dir_name)
        origin = repo.remotes.origin
        try:
            origin.fetch()
        except AssertionError:
            # usually this fails on the first run, try again
            origin.fetch()
        except git.exc.GitCommandError:
            log.critical("%s: Fetch error, this dataset will be left out." % name)
            raise
        # see if we have updates
        if not local_and_remote_are_at_same_commit(repo, origin):
            log.info("%s: Repo has new commits, updating local copy." % name)
            try:
                result = origin.pull()[0]
            except git.exc.GitCommandError:
                log.critical("%s: Pull error, this dataset will be left out." % name)
            if result.flags & result.ERROR:
                log.error("%s: Pull error, but going ahead." % name)
        else:
            log.info("%s: No changes." % name)
    else:
        log.info("%s: New repo, cloning." % name)
        repo = git.Repo.clone_from(url, dir_name)

    if not dir_name.endswith('/'):
        dir_name += '/'
    return dir_name


def get_sfnt_dict(font):
    sfnt = {}
    for item in font.sfnt_names:
        sfnt[item[1].lower()] = item[2]
    return sfnt


def add_key_if_exists(dict, sourcedict, key, name=''):
    if not name:
        name = key
    if sourcedict.get(key):
        dict[name] = sourcedict[key]


def find_fonts_and_sources(dirname):
    fonts = []
    sources = []
    for root, dirnames, filenames in os.walk(dirname):
        for extension in ('*.ttf', '*.otf'):
            for filename in fnmatch.filter(filenames, extension):
                fonts.append(os.path.join(root, filename))
        for filename in fnmatch.filter(filenames, '*.sfd'):
            sources.append(os.path.join(root, filename))
        for filename in fnmatch.filter(dirnames, '*.ufo'):
            sources.append(os.path.join(root, filename))
        for filename in fnmatch.filter(dirnames, '*.sfdir'):
            sources.append(os.path.join(root, filename))
    return fonts, sources


def process_fontfile(filename):
    f = fontforge.open(filename)
    sfnt = get_sfnt_dict(f)

    fontdata = {}
    fontdata['family'] = f.familyname
    fontdata['version'] = f.version
    add_key_if_exists(fontdata, sfnt, 'manufacturer')
    add_key_if_exists(fontdata, sfnt, 'designer')
    add_key_if_exists(fontdata, sfnt, 'license')
    add_key_if_exists(fontdata, sfnt, 'license url', name='license_url')
    add_key_if_exists(fontdata, sfnt, 'copyright')

    fontdata['fontname'] = f.fontname
    fontdata['fullname'] = f.fullname
    fontdata['weight'] = f.weight
    fontdata['subfamily'] = sfnt['subfamily']
    fontdata['path'] = filename
    fontdata['md5'] = md5sum(filename)

    return fontdata


def merge_identical_fields(pkgdata):
    '''If all fonts have the same value for a field, use that for the whole package'''
    fields = ['family', 'version', 'manufacturer', 'designer', 'license', 'license_url',
              'copyright']
    fontdicts = pkgdata['fontfiles'] + pkgdata['sourcefiles']
    for field in fields:
        unique_values = list(set([fd.get(field) for fd in fontdicts]))
        if len(unique_values) == 1 and unique_values[0]:
            # identical value, remove from individual fonts and apply to font package
            for fd in fontdicts:
                if fd.get(field):
                    del fd[field]
            pkgdata[field] = unique_values[0]
    return pkgdata


def process_repo(url):
    repo_dir = clone_or_update_repo(url)
    fonts, sources = find_fonts_and_sources(repo_dir)
    pkgdata = {}
    pkgdata['repository'] = url
    pkgdata['fontfiles'] = []
    for filename in fonts:
        fontdata = process_fontfile(filename)
        fontdata['path'] = fontdata['path'].replace(repo_dir, '')
        pkgdata['fontfiles'].append(fontdata)
    pkgdata['sourcefiles'] = []
    for filename in sources:
        fontdata = process_fontfile(filename)
        fontdata['path'] = fontdata['path'].replace(repo_dir, '')
        pkgdata['sourcefiles'].append(fontdata)
    pkgdata = merge_identical_fields(pkgdata)
    if pkgdata.get('family'):
        pkg_name = pkgdata.get('family').lower().replace(' ', '-')
    else:
        pkg_name = url.split('/')[-1].replace('.git', '')
    filename = os.path.join(fontpkg_dir, pkg_name + '.json')
    import codecs
    f = codecs.open(filename, 'w', 'utf-8')
    f.write(json.dumps(pkgdata, indent=2))
    f.close()
    return pkg_name

if __name__ == "__main__":
    import sys
    process_repo(sys.argv[1])
